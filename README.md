# _CSS ANIMATION DESIGN_

#### _Css animation, 2018_

#### By _**Amaka Mbah**_

## Description

_CSS animation creating a crescent moon and a house_

## Setup/Installation Requirements

*_None needed, just a css animation project_


## Known Bugs

_None I can account for but if you do see, you are welcome to contribute_

## Support and contact details

_Feel free to make your contributions_

## Technologies Used

_*HTML*_
_*CSS*_

### Legal

*This software is licensed under the MIT license*

Copyright (c) 2018 **_Amaka Mbah_**